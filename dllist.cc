#include <iostream>
#include "dllist.h"

using namespace std;

DLLElement::DLLElement(void *itemPtr, int sortKey){
    item = itemPtr;
    key = sortKey;
}

DLList::DLList(){
    first = last = NULL;
}

DLList::~DLList(){
    DLLElement *top;
    DLLElement *info;
    top = first;
    int x = 0;
    while(top){
	info = top->next;
	delete []top;
	top = info;
    }
}

void DLList::Prepend(void *item){
    DLLElement *info = new DLLElement(item, 0);
    info->item = item;
    info->prev = NULL;
    if(IsEmpty()){
	info->next = NULL;
	first = last = info;
	return;
    }
    info->key = first->key;
    info->key--;
    DLLElement *tmp;
    tmp = first;
    first = info;
    first->next = tmp;
    tmp->prev = first;
}

void DLList::Append(void *item){
    DLLElement *info = new DLLElement(item, 0);
    info->item = item;
    info->next = NULL;
    if(IsEmpty()){
	info->prev = NULL;
	first = last = info;
	return;
    }
    info->key = last->key;
    info->key++;
    DLLElement *tmp;
    tmp = last;
    last = info;
    last->prev = tmp;
    tmp->next = last;
}

void *DLList::Remove(int *keyPtr){
    if(IsEmpty()){
	return NULL;
    }

    DLLElement *top;
    top = first;
    if(first != last){
	first = first->next;
	first->prev = NULL;
    } else{
	first == last == NULL;
    }
    *keyPtr = top->key;
    void* tmp = top->item;
    delete []top;
    return tmp; 
    
} 

bool DLList::IsEmpty(){
    if(first == NULL && last == NULL){
	return true;
    }
	return false;
}

void DLList::SortedInsert(void *item, int sortKey){
    DLLElement *info = new DLLElement(item, sortKey);
    if(IsEmpty()){
        info->next = NULL;
	info->prev = NULL;
	first = last = info;
        return;
    }
    DLLElement *next = first;
    if(next->key >= sortKey){
	first = info;
	first->next = next;
	first->prev= NULL;
	return;	
    }
    DLLElement *prev = NULL;
    while(next){
	if(next->key >= sortKey){
	    info->next = next;
	    info->prev = prev;
	    next->prev = info;
	    prev->next = info;
	    return;
	}
	prev = next;
	next = next->next;
    }
    last = info;
    prev->next = last;
    last->prev = prev;
    last->next = NULL;
}

void *DLList::SortedRemove(int sortKey){
    DLLElement *top = first;
    if(sortKey == first->key){
	first->next->prev = NULL;
	first = first->next;
	void *tmp = top->item;
	delete []top;
	return tmp;
    }
    while(top != last){
	if(top->key == sortKey){
	    top->next->prev = top->prev;
	    top->prev->next = top->next;
	    void *tmp = top->item;
	    delete []top;
	    return tmp;
	}
	top = top->next;
    }
    if(sortKey == last->key){
	last->prev->next = NULL;
	last = last->prev;
	void *tmp = top->item;
	delete []top;
	return tmp;
    }
    return NULL;
}

void DLList::dump(){
    DLLElement *tmp = first;
    int i = 0;
    while(tmp){
	cout << "key = " << tmp->key << endl;
	tmp = tmp->next;
	i++;	
    }
}
