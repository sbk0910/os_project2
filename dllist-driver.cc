#include <iostream>
#include <cstdlib>

#include "dllist.h"
#include "main.h"
#include "switch.h"
#include "alarm.h"
#include "kernel.h"
#include "scheduler.h"
#include "synch.h"
#include "synchlist.h"
#include "copyright.h"
#include "sysdep.h"

using namespace std;
int N, T;
DLList *list;

void InsertNDLList(int threadN){
    for(int i = 0; i < N; i++){
	int *a = new int;
	*a = rand()%100;
	list->SortedInsert(a, *a);
	cout << "\t\t" << threadN << "th Thread -> Inserted key : " << *a << endl; 
	delete []a;
    }
}

void RemoveNDLList(int threadN){
    for(int i = 0; i < N; i++){
	int *x = new int;
	int *tmp = (int*)list->Remove(x);
	cout << threadN << "th Thread -> Removed key : " << *x << "\titem : " <<  *tmp << endl;
	delete []x;
    }
}

void ListFunction(int threadN){
    InsertNDLList(threadN);
    RemoveNDLList(threadN);
}

void SelfTest(){
    for(int i = 0; i < T; i++){
	Thread *t = new Thread("forked thread");
	t->Fork((VoidFunctionPtr)ListFunction, (void*) i+1);
    }
}

void HW1Test(int argc, char** argv){
    T = -1;
    N = -1;
    for(int i = 0; i < argc; i++){
	if(strcmp(argv[i], "-h") == 0){
	    T = atoi(argv[i+1]);
	    N = atoi(argv[i+2]);
	    i += 2;
	}
    }

    cout << "T : " <<T << "    N : "  << N << endl;
    bool isStart = T>0 && N>0;
    DEBUG(isStart, "Start SelfTest");

    list = new DLList();
    SelfTest();

}
