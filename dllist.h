#ifndef DLLIST_H_
#define DLLIST_H_

class DLLElement{
public:
   DLLElement(void *itemPtr, int sortKey);
   DLLElement *next;
   DLLElement *prev;
   int key;
   void *item;
};

class DLList{
public:
    DLList();
    ~DLList();
    void Prepend(void *item);  // set key = min - 1
    void Append(void *item);   // set key = max + 1
    void *Remove(int *keyPtr);
    bool IsEmpty();
    void SortedInsert(void *item, int sortKey);
    void *SortedRemove(int sortKey);
    void dump();

private:
    DLLElement *first;
    DLLElement *last;
};

void InsertNDLList(int threadN);
void RemoveNDLList(int threadN);
void ListFunction(int threadN);
void SelfTest();
void HW1Test(int argc, char** argv);

#endif
